import {DefaultCrudRepository} from '@loopback/repository';
import {Publication, PublicationRelations} from '../models';
import {DbPasantiaDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class PublicationRepository extends DefaultCrudRepository<
  Publication,
  typeof Publication.prototype.id,
  PublicationRelations
> {
  constructor(
    @inject('datasources.db_pasantia') dataSource: DbPasantiaDataSource,
  ) {
    super(Publication, dataSource);
  }
}
