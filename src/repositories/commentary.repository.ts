import {DefaultCrudRepository} from '@loopback/repository';
import {Commentary, CommentaryRelations} from '../models';
import {DbPasantiaDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class CommentaryRepository extends DefaultCrudRepository<
  Commentary,
  typeof Commentary.prototype.id,
  CommentaryRelations
> {
  constructor(
    @inject('datasources.db_pasantia') dataSource: DbPasantiaDataSource,
  ) {
    super(Commentary, dataSource);
  }
}
