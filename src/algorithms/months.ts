
let months: Array<string> = [
  'May',
  'August',
  'December',
  'March',
  'January',
  'February',
  'July',
  'October',
  'June',
  'April',
  'September',
  'November'
];

function orderMonths(): void{
  let month: string;
  let monthOld: string;
  let index: number;

  for( var i: number = 0; i < months.length; i++ ){
    monthOld = months[i];
    month = getMonth(i);

    index = months.findIndex( (m) => m == month );

    months[i] = months[index];
    months[index] = monthOld;
  }

  console.log( months );
}

function getMonth( numberMonth: number ): string{
  switch( numberMonth ){
    case 0: {
      return 'January';
    }
    case 1: {
      return 'February';
    }
    case 2: {
      return 'March';
    }
    case 3: {
      return 'April';
    }
    case 4: {
      return 'May';
    }
    case 5: {
      return 'June';
    }
    case 6: {
      return 'July';
    }
    case 7: {
      return 'August';
    }
    case 8: {
      return 'September';
    }
    case 9: {
      return 'October';
    }
    case 10: {
      return 'November';
    }
    case 11: {
      return 'December';
    }
  }
  return '';
}

orderMonths();


