import {Entity, model, property} from '@loopback/repository';

@model()
export class Commentary extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  content: string;

  @property({
    type: 'date',
    required: true,
  })
  date: string;


  constructor(data?: Partial<Commentary>) {
    super(data);
  }
}

export interface CommentaryRelations {
  // describe navigational properties here
}

export type CommentaryWithRelations = Commentary & CommentaryRelations;
